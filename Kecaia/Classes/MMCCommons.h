//
//  MMCCommons.h
//  kecaia
//
//  Created by Mario Concilio on 09/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kMMCFontBold        @"OpenSans-Bold"
#define kMMCFontLight       @"OpenSans-Light"
#define kMMCFontExtrabold   @"OpenSans-ExtraBold"
#define kMMCFontRegular     @"OpenSans-Regular"
#define kMMCFontSemibold    @"OpenSans-Semibold"

#define kMMCDomain          @"http://www.kecaia.com.br"
#define kMMCDomainCategory  @"http://www.kecaia.com.br/categoria/%@"
#define kMMCDomainSearch    @"http://www.kecaia.com.br/?s="

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@interface MMCCommons : NSObject

@end
