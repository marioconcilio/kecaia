//
//  UIColor+CustomColor.h
//  kecaia
//
//  Created by Mario Concilio on 09/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColor)

+ (UIColor *)customPink;
+ (UIColor *)customBlack;
+ (UIColor *)customGreen;
+ (UIColor *)customRed;
+ (UIColor *)customBlue;
+ (UIColor *)customWhite;
+ (UIColor *)customGray;

@end
