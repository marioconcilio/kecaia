//
//  UIColor+CustomColor.m
//  kecaia
//
//  Created by Mario Concilio on 09/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import "UIColor+CustomColor.h"

@implementation UIColor (CustomColor)

+ (UIColor *)customPink
{
    return [self colorWithRed:(229.0/255.0) green:(75.0/255.0) blue:(93.0/255.0) alpha:1.0];
}

+ (UIColor *)customBlack
{
    return [self colorWithRed:(52.0/255.0) green:(53.0/255.0) blue:(62.0/255.0) alpha:1.0];
}

+ (UIColor *)customGreen
{
    return [self colorWithRed:(111.0/255.0) green:(171.0/255.0) blue:(67.0/255.0) alpha:1.0];
}

+ (UIColor *)customRed
{
    return [self colorWithRed:(157.0/255.0) green:(38.0/255.0) blue:(29.0/255.0) alpha:1.0];
}

+ (UIColor *)customBlue
{
    return [self colorWithRed:0.0 green:(190.0/255.0) blue:(204.0/255.0) alpha:1.0];
}

+ (UIColor *)customWhite
{
    return [self colorWithRed:(238.0/255.0) green:(238.0/255.0) blue:(238.0/255.0) alpha:1.0];
}

+ (UIColor *)customGray
{
    return [self colorWithRed:(143.0/255.0) green:(161.0/255.0) blue:(167.0/255.0) alpha:1.0];
}

@end
