//
//  MMCAddFavoritesLabel.h
//  Kecaia
//
//  Created by Mario Concilio on 10/05/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMCAddFavoritesLabel : UILabel

- (instancetype)initWithNavigationController:(__weak UINavigationController *)controller;
- (void)showLabel;

@end
