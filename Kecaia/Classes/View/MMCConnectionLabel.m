//
//  MMCConnectionLabel.m
//  Kecaia
//
//  Created by Mario Concilio on 28/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import "MMCConnectionLabel.h"

@implementation MMCConnectionLabel

- (instancetype)initWithNavigationController:(__weak UINavigationController *)controller
{
    self = [super init];
    if (self) {
        self.font = [UIFont fontWithName:kMMCFontLight size:11.0];
        self.textAlignment = NSTextAlignmentCenter;
        self.text = @"Sem conexão com a internet";
        self.alpha = 0;
        
        [controller.view addSubview:self];
        UINavigationBar *navBar = controller.navigationBar;
        [self setTranslatesAutoresizingMaskIntoConstraints:NO];
        NSDictionary *views = NSDictionaryOfVariableBindings(self, navBar);
        [controller.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[self]|"
                                                                                     options:0
                                                                                     metrics:nil
                                                                                       views:views]];
        
        [controller.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[navBar][self(15.0)]"
                                                                                     options:0
                                                                                     metrics:nil
                                                                                       views:views]];
    }
    
    return self;
}

- (void)setStyle:(MMCConnectionLabelStyle)style
{
    switch (style) {
        case MMCConnectionLabelStyleDark:
            self.textColor = [UIColor whiteColor];
            self.backgroundColor = [UIColor customBlack];
            break;
            
        case MMCConnectionLabelStyleLight:
            self.textColor = [UIColor customBlack];
            self.backgroundColor = [UIColor customPink];
            break;
    }
}

- (void)showLabel
{
    [UIView animateWithDuration:0.7
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.alpha = 0.9;
                     }
                     completion:nil];
    
    self.isVisible = YES;
}

- (void)hideLabel
{
    [UIView animateWithDuration:0.7
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.alpha = 0.0;
                     }
                     completion:nil];
    
    self.isVisible = NO;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
