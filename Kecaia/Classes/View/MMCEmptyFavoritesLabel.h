//
//  MMCEmptyFavoritesLabel.h
//  Kecaia
//
//  Created by Mario Concilio on 08/05/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMCEmptyFavoritesLabel : UILabel

- (instancetype)initWithView:(__weak UIView *)view;
- (void)showLabel;
- (void)hideLabel;

@end
