//
//  MMCConnectionLabel.h
//  Kecaia
//
//  Created by Mario Concilio on 28/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MMCConnectionLabelStyle) {
    MMCConnectionLabelStyleDark,
    MMCConnectionLabelStyleLight,
};

@interface MMCConnectionLabel : UILabel

@property (nonatomic) BOOL isVisible;

- (instancetype)initWithNavigationController:(__weak UINavigationController *)controller;
- (void)setStyle:(MMCConnectionLabelStyle)style;
- (void)showLabel;
- (void)hideLabel;

@end
