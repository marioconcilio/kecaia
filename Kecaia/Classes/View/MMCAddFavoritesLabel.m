//
//  MMCAddFavoritesLabel.m
//  Kecaia
//
//  Created by Mario Concilio on 10/05/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import "MMCAddFavoritesLabel.h"

@implementation MMCAddFavoritesLabel

- (instancetype)initWithNavigationController:(__weak UINavigationController *)controller
{
    self = [super init];
    if (self) {
        self.font = [UIFont fontWithName:kMMCFontLight size:14.0];
        self.text = @"Adicionado aos Favoritos";
        self.textAlignment = NSTextAlignmentCenter;
        self.backgroundColor = [UIColor customBlack];
        self.textColor = [UIColor customWhite];
        self.alpha = 0;
        
        [controller.view addSubview:self];
        UIToolbar *toolBar = controller.toolbar;
        [self setTranslatesAutoresizingMaskIntoConstraints:NO];
        NSDictionary *views = NSDictionaryOfVariableBindings(self, toolBar);
        [controller.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[self]|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:views]];
        
        [controller.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[self(25.0)][toolBar]"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:views]];
    }
    
    return self;
}

- (void)showLabel
{
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.alpha = 0.9;
                     }
                     completion:nil];
    
    [self performSelector:@selector(hideLabel) withObject:nil afterDelay:2.0];
}

- (void)hideLabel
{
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.alpha = 0.0;
                     }
                     completion:nil];
}

@end
