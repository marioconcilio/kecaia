//
//  MMCOfferCell.h
//  kecaia
//
//  Created by Mario Concilio on 11/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMCOfferCell : UITableViewCell

@property (strong, nonatomic) UITableViewController *controller;
@property (weak, nonatomic) IBOutlet UIImageView *rowImage;
@property (weak, nonatomic) IBOutlet UILabel *priceBeforeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceAfterLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

- (void)setTitle:(NSString *)title;
- (void)setDiscount:(NSString *)discount;
- (void)setPriceBefore:(NSString *)price;
- (void)setPriceAfter:(NSString *)price;

@end
