//
//  MMCEmptyFavoritesLabel.m
//  Kecaia
//
//  Created by Mario Concilio on 08/05/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import "MMCEmptyFavoritesLabel.h"

@implementation MMCEmptyFavoritesLabel

- (instancetype)initWithView:(__weak UIView *)view
{
    self = [super init];
    if (self) {
        self.text = @"Você ainda não possui nenhum favorito";
        self.font = [UIFont fontWithName:kMMCFontLight size:16.0];
        self.textColor = [UIColor lightGrayColor];
        self.numberOfLines = 0;
        self.textAlignment = NSTextAlignmentCenter;
        self.alpha = 0;
        
        [view addSubview:self];
        [self setTranslatesAutoresizingMaskIntoConstraints:NO];
        NSDictionary *views = NSDictionaryOfVariableBindings(self, view);
        [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[self]|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:views]];

        [view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[self]|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:views]];
    }
    
    return self;
}

- (void)showLabel
{
    [UIView animateWithDuration:0.7
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.alpha = 1.0;
                     }
                     completion:nil];
    
//    self.isVisible = YES;
}

- (void)hideLabel
{
    self.alpha = 0.0;
//    self.isVisible = NO;
}

@end
