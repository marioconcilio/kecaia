//
//  MMCOfferCell.m
//  kecaia
//
//  Created by Mario Concilio on 11/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import "MMCOfferCell.h"

@implementation MMCOfferCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if (selected) {
        [self.controller performSegueWithIdentifier:@"WebSegue" sender:self];
    }
}

#pragma mark - Setup Cell Methods
- (void)setTitle:(NSString *)title
{
    self.titleLabel.font = [UIFont fontWithName:kMMCFontLight size:14.0f];
    self.titleLabel.textColor = [UIColor customBlack];
    self.titleLabel.text = title;
}

- (void)setDiscount:(NSString *)discount
{
    self.discountLabel.font = [UIFont fontWithName:kMMCFontBold size:14.0f];
    self.discountLabel.textColor = [UIColor whiteColor];
    self.discountLabel.text = discount;
}

- (void)setPriceBefore:(NSString *)price
{
    if (!price) {
//        price = @"";
        self.priceBeforeLabel.textColor = [UIColor clearColor];
        return;
    }
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:price];
    [attString addAttribute:NSStrikethroughStyleAttributeName value:@1 range:NSMakeRange(0, [attString length])];
    self.priceBeforeLabel.font = [UIFont fontWithName:kMMCFontBold size:14.0f];
    self.priceBeforeLabel.textColor = [UIColor customBlack];
    self.priceBeforeLabel.attributedText = attString;
}

- (void)setPriceAfter:(NSString *)price
{
    self.priceAfterLabel.font = [UIFont fontWithName:kMMCFontBold size:14.0f];
    self.priceAfterLabel.textColor = [UIColor customRed];
    self.priceAfterLabel.text = price;
}

/*
- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    if (highlighted) {
        [self.image applyTintEffectWithColor:[UIColor customBlue]];
        self.rowImage.image = self.image;
    }
    else {
        self.rowImage.image = self.image;
    }
}
*/

@end
