//
//  MMCOffer.m
//  kecaia
//
//  Created by Mario Concilio on 12/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import "MMCOffer.h"
#import "MMCOfferManaged.h"

@implementation MMCOffer

+ (instancetype)offer
{
    MMCOffer __autoreleasing *offer = [[MMCOffer alloc] init];
    return offer;
}

+ (instancetype)offerWithOfferManaged:(MMCOfferManaged *)offerManaged
{
    MMCOffer __autoreleasing *offer = [[MMCOffer alloc] init];
    if (offer) {
        offer.title = offerManaged.title;
        offer.thumbnail = offerManaged.thumbnail;
        offer.storeLink = offerManaged.storeLink;
        offer.offerLink = offerManaged.offerLink;
        offer.priceBefore = offerManaged.priceBefore;
        offer.priceAfter = offerManaged.priceAfter;
        offer.discount = offerManaged.discount;
    }
    
    return offer;
}

- (NSURL *)thumbnailURL
{
    return [NSURL URLWithString:self.thumbnail];
}

- (NSURL *)storeURL
{
    return [NSURL URLWithString:self.storeLink];
}

- (NSURL *)offerURL
{
    return [NSURL URLWithString:self.offerLink];
}

@end
