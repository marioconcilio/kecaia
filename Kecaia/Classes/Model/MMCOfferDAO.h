//
//  MMCOfferDAO.h
//  kecaia
//
//  Created by Mario Concilio on 17/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

@class MMCOffer;
@interface MMCOfferDAO : NSObject

+ (void)addOfferToCoreData:(MMCOffer *)offer;

+ (NSArray *)listOffersFromCoreData;

+ (void)removeOffersFromCoreData:(NSArray *)offers;
+ (void)removeOfferFromCoreData:(MMCOffer *)offer;

+ (BOOL)existsOfferInCoreData:(MMCOffer *)offer;

+ (NSArray *)listOffersFromHTMLString:(NSString *)htmlString;

@end
