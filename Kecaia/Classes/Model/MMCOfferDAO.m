//
//  MMCOfferDAO.m
//  kecaia
//
//  Created by Mario Concilio on 17/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import "MMCOfferDAO.h"
#import "MMCOffer.h"
#import "MMCOfferManaged.h"
#import "MMCAppDelegate.h"

#define kThumbnailIndex     10
#define kTitleIndex         12
#define kOfferLinkIndex     24
#define kStoreLinkIndex     34
#define kDiscountIndex      64
#define kPriceBeforeIndex   70
#define kPriceAfterIndex    76

#define kMMCStringSeparator @"<div\nclass=\"product\">"

@implementation MMCOfferDAO

+ (void)addOfferToCoreData:(MMCOffer *)offer
{
    MMCAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    __unused MMCOfferManaged *offerManaged = [MMCOfferManaged insertNewObjectForEntityForName:@"OfferManaged"
                                                              inManagedObjectContext:context
                                                                            forOffer:offer];
    
//    [appDelegate saveContext];
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"error saving managed offer: %@", [error localizedDescription]);
    }
}

+ (NSArray *)listOffersFromCoreData
{
    MMCAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"OfferManaged"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *fetchedRequest = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error listing offers: %@", [error localizedDescription]);
    }
    
    return fetchedRequest;
}

+ (void)removeOffersFromCoreData:(NSArray *)offers
{
    if (offers.count == 0) {
        return;
    }
    
    MMCAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    
    for (MMCOfferManaged *offer in offers) {
        [context deleteObject:offer];
    }

    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"error deleting multiple managed offers: %@", [error localizedDescription]);
    }
}

+ (void)removeOfferFromCoreData:(MMCOffer *)offer
{
    MMCAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"OfferManaged"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title = %@", offer.title];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedRequest = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {
        NSLog(@"error listing offers: %@", [error localizedDescription]);
        return;
    }
    
    if (fetchedRequest.count == 1) {
        [context deleteObject:[fetchedRequest objectAtIndex:0]];
    }
    
    if (![context save:&error]) {
        NSLog(@"error deleting single managed offer: %@", [error localizedDescription]);
    }
}

+ (BOOL)existsOfferInCoreData:(MMCOffer *)offer
{
    MMCAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];

    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"OfferManaged"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title = %@", offer.title];
    [fetchRequest setPredicate:predicate];
    NSError *error = nil;
    NSArray *fetchedRequest = [context executeFetchRequest:fetchRequest error:&error];
    
    if (error) {
        NSLog(@"error listing offers: %@", [error localizedDescription]);
        return NO;
    }
    
    if (fetchedRequest.count > 0) {
        return YES;
    }
    
    return NO;
}

+ (NSArray *)listOffersFromHTMLString:(NSString *)htmlString
{
    @autoreleasepool {
        NSMutableArray *strOffers = [NSMutableArray arrayWithArray:[htmlString componentsSeparatedByString:kMMCStringSeparator]];
        [strOffers removeObjectAtIndex:0];
        NSUInteger size = [strOffers count];
        
        NSMutableArray *arrayOffers = [NSMutableArray arrayWithCapacity:size];
        for (NSString *str in strOffers) {
            @autoreleasepool {
                NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:@"<>\""];
                NSArray *array = [str componentsSeparatedByCharactersInSet:charSet];
                
                MMCOffer *offer = [MMCOffer offer];
                offer.thumbnail = [array objectAtIndex:kThumbnailIndex];
                offer.offerLink = [array objectAtIndex:kOfferLinkIndex];
                offer.storeLink = [array objectAtIndex:kStoreLinkIndex];
                offer.discount = [array objectAtIndex:kDiscountIndex];
                offer.priceAfter = [array objectAtIndex:kPriceAfterIndex];
                offer.priceBefore = [array objectAtIndex:kPriceBeforeIndex];
                
                NSString *title = [[array objectAtIndex:kTitleIndex] stringByReplacingOccurrencesOfString:@"&#8211;" withString:@"-"];
                offer.title = title;
                
                [arrayOffers addObject:offer];
            }
        }
        
        return arrayOffers;
    }
}

@end