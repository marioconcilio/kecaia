//
//  MMCOfferManaged.m
//  Kecaia
//
//  Created by Mario Concilio on 28/04/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import "MMCOfferManaged.h"
#import "MMCOffer.h"

@implementation MMCOfferManaged : NSManagedObject

@dynamic thumbnail;
@dynamic storeLink;
@dynamic offerLink;
@dynamic title;
@dynamic priceBefore;
@dynamic priceAfter;
@dynamic discount;

+ (instancetype)insertNewObjectForEntityForName:(NSString *)entityName
                         inManagedObjectContext:(NSManagedObjectContext *)context
                                       forOffer:(MMCOffer *)offer
{
    MMCOfferManaged __autoreleasing *offerManaged = [NSEntityDescription insertNewObjectForEntityForName:entityName
                                                                                  inManagedObjectContext:context];
    if (offerManaged) {
        offerManaged.title = offer.title;
        offerManaged.thumbnail = offer.thumbnail;
        offerManaged.storeLink = offer.storeLink;
        offerManaged.offerLink = offer.offerLink;
        offerManaged.priceBefore = offer.priceBefore;
        offerManaged.priceAfter = offer.priceAfter;
        offerManaged.discount = offer.discount;
    }
    
    return offerManaged;
}

- (NSURL *)thumbnailURL
{
    return [NSURL URLWithString:self.thumbnail];
}

- (NSURL *)storeURL
{
    return [NSURL URLWithString:self.storeLink];
}

- (NSURL *)offerURL
{
    return [NSURL URLWithString:self.offerLink];
}

@end
