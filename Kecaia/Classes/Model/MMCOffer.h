//
//  MMCOffer.h
//  kecaia
//
//  Created by Mario Concilio on 12/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

@class MMCOfferManaged;
@interface MMCOffer : NSObject

@property (copy, nonatomic) NSString *thumbnail;
@property (copy, nonatomic) NSString *storeLink;
@property (copy, nonatomic) NSString *offerLink;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *priceBefore;
@property (copy, nonatomic) NSString *priceAfter;
@property (copy, nonatomic) NSString *discount;

+ (instancetype)offer;
+ (instancetype)offerWithOfferManaged:(MMCOfferManaged *)offerManaged;
- (NSURL *)thumbnailURL;
- (NSURL *)storeURL;
- (NSURL *)offerURL;

@end
