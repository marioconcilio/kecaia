//
//  MMCOfferManaged.h
//  Kecaia
//
//  Created by Mario Concilio on 28/04/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

@class MMCOffer;
@interface MMCOfferManaged : NSManagedObject

@property (copy, nonatomic) NSString *thumbnail;
@property (copy, nonatomic) NSString *storeLink;
@property (copy, nonatomic) NSString *offerLink;
@property (copy, nonatomic) NSString *title;
@property (copy, nonatomic) NSString *priceBefore;
@property (copy, nonatomic) NSString *priceAfter;
@property (copy, nonatomic) NSString *discount;

+ (instancetype)insertNewObjectForEntityForName:(NSString *)entityName
                         inManagedObjectContext:(NSManagedObjectContext *)context
                                       forOffer:(MMCOffer *)offer;

- (NSURL *)thumbnailURL;
- (NSURL *)storeURL;
- (NSURL *)offerURL;

@end
