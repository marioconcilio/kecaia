//
//  MMCFavoritesTableController.m
//  Kecaia
//
//  Created by Mario Concilio on 28/04/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <AFNetworking/UIImageView+AFNetworking.h>

#import "MMCFavoritesTableController.h"
#import "MMCOfferCell.h"
#import "MMCOfferManaged.h"
#import "MMCOffer.h"
#import "MMCOfferDAO.h"
#import "MMCEmptyFavoritesLabel.h"
#import "MMCWebViewController.h"

@implementation MMCFavoritesTableController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepareViewTitle:@"Meus Favoritos" forStyle:MMCTitleStyleDark];
    [self.tableView registerNib:[UINib nibWithNibName:@"MMCOfferCell" bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:@"OfferCell"];
    
    self.offersToRemove = [[NSMutableArray alloc] init];
    self.emptyLabel = [[MMCEmptyFavoritesLabel alloc] initWithView:self.navigationController.view];

    self.editButtonItem.title = @"Editar";
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSArray *array = [MMCOfferDAO listOffersFromCoreData];
    self.list = [[NSMutableArray alloc] initWithArray:array];
    [self.tableView reloadData];
    
    if (self.list.count == 0) {
        [self.emptyLabel showLabel];
        self.tableView.scrollEnabled = NO;
    }
    else {
        [self.emptyLabel hideLabel];
        self.tableView.scrollEnabled = YES;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [MMCOfferDAO removeOffersFromCoreData:self.offersToRemove];
    [self.offersToRemove removeAllObjects];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"WebSegue"]) {
        MMCWebViewController *wvc = (MMCWebViewController *) segue.destinationViewController;
        NSIndexPath *path = [self.tableView indexPathForSelectedRow];
        MMCOfferManaged *offerManaged = [self.list objectAtIndex:path.row];
        
        MMCOffer *offer = [MMCOffer offerWithOfferManaged:offerManaged];
        wvc.offer = offer;
    }
}

#pragma mark - Table View Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"OfferCell";
    MMCOfferCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [cell.rowImage cancelImageRequestOperation];
    
    // Configure the cell...
    // data to cell
    NSUInteger row = [indexPath row];
    MMCOfferManaged *offer = [self.list objectAtIndex:row];
    
    // image
    __weak MMCOfferCell *weakCell = cell;
    NSURLRequest *request = [NSURLRequest requestWithURL:[offer thumbnailURL]];
    [cell.rowImage setImageWithURLRequest:request
                         placeholderImage:nil
                                  success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                      [UIView transitionWithView:weakCell.rowImage
                                                        duration:0.2
                                                         options:UIViewAnimationOptionTransitionCrossDissolve
                                                      animations:^{
                                                          weakCell.rowImage.image = image;
                                                      }
                                                      completion:nil];
                                  }
                                  failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                      
                                  }];
    
    [cell setTitle:offer.title];
    [cell setDiscount:offer.discount];
    [cell setPriceBefore:offer.priceBefore];
    [cell setPriceAfter:offer.priceAfter];
    
    UIView *selectionView = [[UIView alloc] init];
    selectionView.backgroundColor = [UIColor customBlue];
    selectionView.layer.masksToBounds = YES;
    cell.selectedBackgroundView = selectionView;
    
    cell.controller = self;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.offersToRemove addObject:[self.list objectAtIndex:indexPath.row]];    
    [self.list removeObjectAtIndex:indexPath.row];
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
    if (self.list.count == 0) {
        [self.emptyLabel showLabel];
        self.tableView.scrollEnabled = NO;
    }
}

@end
