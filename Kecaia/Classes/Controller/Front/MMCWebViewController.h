//
//  MMCWebViewController.h
//  kecaia
//
//  Created by Mario Concilio on 14/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NJKScrollFullScreen/NJKScrollFullScreen.h>
#import <NJKWebViewProgress/NJKWebViewProgress.h>

@class MMCOffer;
@class MMCAddFavoritesLabel;
@interface MMCWebViewController : UIViewController <UIWebViewDelegate, UIActionSheetDelegate, NJKScrollFullscreenDelegate, NJKWebViewProgressDelegate>

@property (nonatomic) BOOL isFavorite;
@property (strong, nonatomic) MMCAddFavoritesLabel *addLabel;
@property (strong, nonatomic) MMCOffer *offer;
@property (strong, nonatomic) NSURLSession *session;
@property (strong, nonatomic) NSURLSessionDataTask *dataTask;
@property (strong, nonatomic) NJKScrollFullScreen *scrollProxy;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *forwardButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *bookmarkButton;

- (void)loadRequest;
- (IBAction)navigatePage:(id)sender;
- (IBAction)reloadPage:(id)sender;
- (IBAction)bookmarkPage:(id)sender;
- (IBAction)openInBrowser:(id)sender;

@end
