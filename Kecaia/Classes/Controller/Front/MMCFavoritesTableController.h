//
//  MMCFavoritesTableController.h
//  Kecaia
//
//  Created by Mario Concilio on 28/04/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMCFrontTableController.h"

@class MMCEmptyFavoritesLabel;
@interface MMCFavoritesTableController : MMCFrontTableController

@property (nonatomic, strong) MMCEmptyFavoritesLabel *emptyLabel;
@property (nonatomic, strong) NSMutableArray *list;
@property (nonatomic, strong) NSMutableArray *offersToRemove;

@end
