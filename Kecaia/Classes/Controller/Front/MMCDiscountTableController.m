//
//  MMCDiscountTableController.m
//  Kecaia
//
//  Created by Mario Concilio on 13/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import "MMCDiscountTableController.h"
#import "MMCOfferDAO.h"

@implementation MMCDiscountTableController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self prepareViewTitle:@"Maiores Descontos" forStyle:MMCTitleStyleDark];
    self.navigationItem.title = @"Descontos";
    
    self.url = [NSURL URLWithString:@"http://www.kecaia.com.br/maiores-descontos"];
    [self prepareTableContents];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
