//
//  MMCDiscountTableController.h
//  Kecaia
//
//  Created by Mario Concilio on 13/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMCOfferTableController.h"

@interface MMCDiscountTableController : MMCOfferTableController

@end
