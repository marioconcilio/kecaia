//
//  MMCCategoriesTableController.m
//  Kecaia
//
//  Created by Mario Concilio on 10/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <SWRevealViewController/SWRevealViewController.h>

#import "MMCCategoriesTableController.h"
#import "MMCSubcategoriesTableController.h"

@implementation MMCCategoriesTableController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self prepareViewTitle:@"Ofertas Moda" forStyle:MMCTitleStyleDark];
    self.navigationItem.title = @"Ofertas";
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"offers" ofType:@"plist"];
    self.dictionary = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    NSSortDescriptor *desc = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
    self.list = [[self.dictionary allKeys] sortedArrayUsingDescriptors:@[desc]];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
    headerLabel.text = @" Moda e Acessórios";
    headerLabel.font = [UIFont fontWithName:kMMCFontLight size:12.0];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor customBlack];
    self.tableView.tableHeaderView = headerLabel;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"CategorySegue"]) {
        MMCSubcategoriesTableController *scvc = (MMCSubcategoriesTableController *) [segue destinationViewController];
        NSIndexPath *path = [self.tableView indexPathForSelectedRow];
        NSString *key = [self.list objectAtIndex:path.row];
        scvc.dictionary = [self.dictionary objectForKey:key];
        [scvc prepareViewTitle:key forStyle:MMCTitleStyleDark];
        scvc.navigationItem.title = key;
    }
}

#pragma mark - Table View Data Source
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CategoryCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.font = [UIFont fontWithName:kMMCFontLight size:18.0];
    
    NSUInteger row = [indexPath row];
    NSString *name = [self.list objectAtIndex:row];
    cell.textLabel.textColor = [UIColor customBlack];
    cell.textLabel.text = name;
    
    UIView *selectionView = [[UIView alloc] init];
    selectionView.backgroundColor = [UIColor customBlue];
    selectionView.layer.masksToBounds = YES;
    cell.selectedBackgroundView = selectionView;
    
    return cell;
}

#pragma mark - ScrollView
- (void)scrollFullScreen:(NJKScrollFullScreen *)proxy scrollViewDidScrollUp:(CGFloat)deltaY
{
}

- (void)scrollFullScreen:(NJKScrollFullScreen *)proxy scrollViewDidScrollDown:(CGFloat)deltaY
{
}

- (void)scrollFullScreenScrollViewDidEndDraggingScrollUp:(NJKScrollFullScreen *)proxy
{
}

- (void)scrollFullScreenScrollViewDidEndDraggingScrollDown:(NJKScrollFullScreen *)proxy
{
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
}

@end
