//
//  MMCSubcategoriesTableController.m
//  Kecaia
//
//  Created by Mario Concilio on 03/04/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import "MMCSubcategoriesTableController.h"
#import "MMCTabBarController.h"
#import "MMCOfferTableController.h"

@implementation MMCSubcategoriesTableController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSSortDescriptor *desc = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
    self.list = [[self.dictionary allKeys] sortedArrayUsingDescriptors:@[desc]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [self.revealViewController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"OffersSegue"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSString *key = [self.list objectAtIndex:indexPath.row];
        NSString *path = [self.dictionary objectForKey:key];
        NSString *stringURL = [NSString stringWithFormat:kMMCDomainCategory, path];
        
        MMCOfferTableController *otvc = (MMCOfferTableController *) segue.destinationViewController;
        otvc.url = [NSURL URLWithString:stringURL];
        [otvc prepareViewTitle:key forStyle:MMCTitleStyleDark];
        [otvc prepareTableContents];
    }
}

#pragma mark - Table View Data Source
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CategoryCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.font = [UIFont fontWithName:kMMCFontLight size:18.0];
    
    NSUInteger row = [indexPath row];
    NSString *name = [self.list objectAtIndex:row];
    cell.textLabel.textColor = [UIColor customBlack];
    cell.textLabel.text = name;
    
    UIView *selectionView = [[UIView alloc] init];
    selectionView.backgroundColor = [UIColor customBlue];
    selectionView.layer.masksToBounds = YES;
    cell.selectedBackgroundView = selectionView;
    
    return cell;
}

@end
