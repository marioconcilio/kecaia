//
//  MMCFrontTableController.m
//  Kecaia
//
//  Created by Mario Concilio on 27/04/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <SWRevealViewController/SWRevealViewController.h>
#import "MMCFrontTableController.h"

@implementation MMCFrontTableController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.revealViewController.delegate = self;
    //    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    UIBarButtonItem *sideBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch
                                                                                   target:self.revealViewController
                                                                                   action:@selector(rightRevealToggle:)];
    self.navigationItem.rightBarButtonItem = sideBarButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
