//
//  MMCTabBarController.m
//  Kecaia
//
//  Created by Mario Concilio on 02/04/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import "MMCTabBarController.h"

@implementation MMCTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITabBarItem *tabNovidades = [self.tabBar.items objectAtIndex:0];
    UITabBarItem *tabDescontos = [self.tabBar.items objectAtIndex:1];
    UITabBarItem *tabBaratos = [self.tabBar.items objectAtIndex:2];
    UITabBarItem *tabFavoritos = [self.tabBar.items objectAtIndex:3];
    UITabBarItem *tabOfertas = [self.tabBar.items objectAtIndex:4];
    
    tabNovidades.image = [[UIImage imageNamed:@"time"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabNovidades.selectedImage = [[UIImage imageNamed:@"time_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    tabDescontos.image = [[UIImage imageNamed:@"sale"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabDescontos.selectedImage = [[UIImage imageNamed:@"sale_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    tabBaratos.image = [[UIImage imageNamed:@"tag"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabBaratos.selectedImage = [[UIImage imageNamed:@"tag_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    tabFavoritos.image = [[UIImage imageNamed:@"star"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabFavoritos.selectedImage = [[UIImage imageNamed:@"star_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    tabOfertas.image = [[UIImage imageNamed:@"list"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    tabOfertas.selectedImage = [[UIImage imageNamed:@"list_selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    self.tabBar.backgroundImage = [UIImage imageNamed:@"tabbar"];
    
    // deprecated in 7.0
//    [tabNovidades setFinishedSelectedImage:[UIImage imageNamed:@"sale_selected"] withFinishedUnselectedImage:[UIImage imageNamed:@"sale"]];
//    [tabDescontos setFinishedSelectedImage:[UIImage imageNamed:@"chart_selected"] withFinishedUnselectedImage:[UIImage imageNamed:@"chart"]];
//    [tabBaratos setFinishedSelectedImage:[UIImage imageNamed:@"tag_selected"] withFinishedUnselectedImage:[UIImage imageNamed:@"tag"]];
//    self.tabBar.selectedImageTintColor = [UIColor customPink];
//    self.tabBar.tintColor = [UIColor customBlack];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
