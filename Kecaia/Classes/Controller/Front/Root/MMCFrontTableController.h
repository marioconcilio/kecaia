//
//  MMCFrontTableController.h
//  Kecaia
//
//  Created by Mario Concilio on 27/04/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWRevealViewController/SWRevealViewController.h>

#import "MMCAbstractTableController.h"

@interface MMCFrontTableController : MMCAbstractTableController <SWRevealViewControllerDelegate>

@end
