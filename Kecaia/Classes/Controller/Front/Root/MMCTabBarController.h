//
//  MMCTabBarController.h
//  Kecaia
//
//  Created by Mario Concilio on 02/04/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMCTabBarController : UITabBarController

@end
