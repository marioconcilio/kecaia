//
//  MMCFrontNavigationController.m
//  Kecaia
//
//  Created by Mario Concilio on 03/04/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import "MMCFrontNavigationController.h"
#import "MMCConnectionLabel.h"

@implementation MMCFrontNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar"] forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.tintColor = [UIColor customPink];
    [self.conn setStyle:MMCConnectionLabelStyleDark];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
