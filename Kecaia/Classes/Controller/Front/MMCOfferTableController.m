//
//  MMCOfferTableController.m
//  Kecaia
//
//  Created by Mario Concilio on 12/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <AFNetworking/UIImageView+AFNetworking.h>

#import "MMCOfferTableController.h"
#import "MMCOfferCell.h"
#import "MMCOffer.h"
#import "MMCOfferDAO.h"
#import "MMCWebViewController.h"
#import "MMCAbstractNavigationController.h"

@implementation MMCOfferTableController

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    
    if (self) {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        config.timeoutIntervalForRequest = 30.0;
        config.timeoutIntervalForResource = 60.0;
        _session = [NSURLSession sessionWithConfiguration:config];
    }
    
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.tableView registerNib:[UINib nibWithNibName:@"MMCOfferCell" bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:@"OfferCell"];

    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.tintColor = [UIColor customPink];
    [self.refreshControl addTarget:self
                            action:@selector(prepareTableContents)
                  forControlEvents:UIControlEventValueChanged];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"WebSegue"]) {
        MMCWebViewController *wvc = (MMCWebViewController *) [segue destinationViewController];
        NSIndexPath *path = [self.tableView indexPathForSelectedRow];
        MMCOffer *offer = [self.list objectAtIndex:path.row];
        wvc.offer = offer;
        [wvc loadRequest];
    }
}

#pragma mark - Helper Methods
- (void)prepareTableContents
{
//#warning Loading Resources From Local HTML File
//    self.url = [[NSBundle mainBundle] URLForResource:@"teste" withExtension:@"html"];
    __weak typeof(self) weakSelf = self;
    self.dataTask = [self.session dataTaskWithURL:self.url
                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                    if (!error) {
                                        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse *) response;
                                        if (httpResp.statusCode == 200) {
                                            MMCLog(@"http status code ok");
                                            NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                            weakSelf.list = [MMCOfferDAO listOffersFromHTMLString:str];
                                            
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                [weakSelf refreshActivity:NO];
                                                
                                                MMCAbstractNavigationController *nav = (MMCAbstractNavigationController *) weakSelf.navigationController;
                                                [nav hideNotConnected];
                                                [weakSelf.tableView reloadData];
                                            });
                                        }
                                        else {
                                            NSLog(@"error http status code %ld", (long)httpResp.statusCode);
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                [weakSelf refreshActivity:NO];
                                                MMCAbstractNavigationController *nav = (MMCAbstractNavigationController *) weakSelf.navigationController;
                                                [nav showNotConnected];
                                            });
                                        }
                                    }
                                    else {
                                        NSLog(@"error data task %@", [error localizedDescription]);
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [weakSelf refreshActivity:NO];
                                            MMCAbstractNavigationController *nav = (MMCAbstractNavigationController *) weakSelf.navigationController;
                                            [nav showNotConnected];
                                        });
                                    }
                                }];
    [self.dataTask resume];
    
    if (!self.refreshControl.isRefreshing) {
        CGFloat height = self.refreshControl.frame.size.height * 2;
        [self.tableView setContentOffset:CGPointMake(0, -height) animated:YES];
        [self refreshActivity:YES];
    }
}

- (void)refreshActivity:(BOOL)refresh
{
    if (refresh) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
        [self.refreshControl beginRefreshing];
    }
    else {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [self.refreshControl endRefreshing];
    }
}

#pragma mark - Table View Data Source
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"OfferCell";
    MMCOfferCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [cell.rowImage cancelImageRequestOperation];
    
    // Configure the cell...
    // data to cell
    NSUInteger row = [indexPath row];
    MMCOffer *offer = [self.list objectAtIndex:row];
    
    // image
    __weak MMCOfferCell *weakCell = cell;
    NSURLRequest *request = [NSURLRequest requestWithURL:[offer thumbnailURL]];
    [cell.rowImage setImageWithURLRequest:request
                         placeholderImage:nil
                                  success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                      [UIView transitionWithView:weakCell.rowImage
                                                        duration:0.2
                                                         options:UIViewAnimationOptionTransitionCrossDissolve
                                                      animations:^{
                                                          weakCell.rowImage.image = image;
                                                      }
                                                      completion:nil];
                                  }
                                  failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                      
                                  }];
    
    [cell setTitle:offer.title];
    [cell setDiscount:offer.discount];
    [cell setPriceBefore:offer.priceBefore];
    [cell setPriceAfter:offer.priceAfter];
    
    UIView *selectionView = [[UIView alloc] init];
    selectionView.backgroundColor = [UIColor customBlue];
    selectionView.layer.masksToBounds = YES;
    cell.selectedBackgroundView = selectionView;
    
    cell.controller = self;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0;
}

@end
