//
//  MMCOfferTableController.h
//  kecaia
//
//  Created by Mario Concilio on 12/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMCFrontTableController.h"

@interface MMCOfferTableController : MMCFrontTableController

@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) NSURLSession *session;
@property (strong, nonatomic) NSURLSessionDataTask *dataTask;
@property (strong, nonatomic) UIRefreshControl *refreshControl;

- (void)prepareTableContents;
- (void)refreshActivity:(BOOL)refresh;

@end
