//
//  MMCCategoriesTableController.h
//  Kecaia
//
//  Created by Mario Concilio on 10/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMCFrontTableController.h"

@interface MMCCategoriesTableController : MMCFrontTableController

@property (nonatomic, strong) NSDictionary *dictionary;

@end
