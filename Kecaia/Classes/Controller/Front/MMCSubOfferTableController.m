//
//  MMCSubOfferTableController.m
//  Kecaia
//
//  Created by Mario Concilio on 29/04/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import "MMCSubOfferTableController.h"
#import "MMCAbstractNavigationController.h"

@implementation MMCSubOfferTableController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.dataTask cancel];
    [self refreshActivity:NO];
    MMCAbstractNavigationController *nav = (MMCAbstractNavigationController *)self.navigationController;
    [nav hideNotConnected];
}

@end
