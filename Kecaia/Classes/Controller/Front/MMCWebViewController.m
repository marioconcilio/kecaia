//
//  MMCWebViewController.m
//  kecaia
//
//  Created by Mario Concilio on 14/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <NJKScrollFullScreen/UIViewController+NJKFullScreenSupport.h>
#import <NJKWebViewProgress/NJKWebViewProgress.h>
#import <NJKWebViewProgress/NJKWebViewProgressView.h>

#import "MMCWebViewController.h"
#import "MMCConnectionLabel.h"
#import "MMCOffer.h"
#import "MMCOfferDAO.h"
#import "MMCOfferTableController.h"
#import "MMCFrontNavigationController.h"
#import "MMCAddFavoritesLabel.h"
#define kBackButton     1
#define kForwardButton  2

@interface MMCWebViewController ()
{
    NJKWebViewProgressView *progressView;
    NJKWebViewProgress *progressProxy;
}
@end

@implementation MMCWebViewController

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    
    if (self) {
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        config.timeoutIntervalForRequest = 30.0;
        config.timeoutIntervalForResource = 60.0;
        _session = [NSURLSession sessionWithConfiguration:config];
    }
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.scrollProxy = [[NJKScrollFullScreen alloc] initWithForwardTarget:self.webView];
    self.webView.scrollView.delegate = (id) self.scrollProxy;
    self.scrollProxy.delegate = self;
    
//    self.webView.delegate = self;
    self.webView.backgroundColor = [UIColor clearColor];
    self.webView.opaque = NO;
    
    [self.navigationController setToolbarHidden:NO animated:YES];
    self.addLabel = [[MMCAddFavoritesLabel alloc] initWithNavigationController:self.navigationController];
    
    progressProxy = [[NJKWebViewProgress alloc] init];
    self.webView.delegate = progressProxy;
    progressProxy.webViewProxyDelegate = self;
    progressProxy.progressDelegate = self;
    
    CGFloat progressBarHeight = 2.f;
    CGRect navigaitonBarBounds = self.navigationController.navigationBar.bounds;
    CGRect barFrame = CGRectMake(0, navigaitonBarBounds.size.height - progressBarHeight, navigaitonBarBounds.size.width, progressBarHeight);
    progressView = [[NJKWebViewProgressView alloc] initWithFrame:barFrame];
    progressView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar addSubview:progressView];
    progressView.progress = 0.0f;
    
    if ([self.navigationController isMemberOfClass:[MMCFrontNavigationController class]]) {
        [self.navigationController.toolbar setBackgroundImage:[UIImage imageNamed:@"tabbar"]
                                           forToolbarPosition:UIBarPositionBottom
                                                   barMetrics:UIBarMetricsDefault];
        
        for (UIBarButtonItem *btn in self.toolbarItems) {
            btn.tintColor = [UIColor customPink];
        }
    }
    else {
        [self.navigationController.toolbar setBackgroundImage:[UIImage imageNamed:@"navbar2"]
                                           forToolbarPosition:UIBarPositionBottom
                                                   barMetrics:UIBarMetricsDefault];
        
        for (UIBarButtonItem *btn in self.toolbarItems) {
            btn.tintColor = [UIColor customWhite];
        }
    }
    
    if ([MMCOfferDAO existsOfferInCoreData:self.offer]) {
        self.bookmarkButton.image = [UIImage imageNamed:@"star_selected"];
        self.isFavorite = YES;
    }
    else {
        self.bookmarkButton.image = [UIImage imageNamed:@"star"];
        self.isFavorite = NO;
    }
    
    [self showNavigationBar:YES];
    [self showToolbar:YES];
//    [self.navigationController setToolbarHidden:NO animated:YES];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setToolbarHidden:YES animated:YES];
    [progressView removeFromSuperview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions
- (IBAction)navigatePage:(id)sender
{
    UIBarButtonItem *btn = (UIBarButtonItem *) sender;
    switch (btn.tag) {
        case kBackButton:
            [self.webView goBack];
            break;
            
        case kForwardButton:
            [self.webView goForward];
            break;
    }
}

- (IBAction)reloadPage:(id)sender
{
    [self.webView reload];
}

- (IBAction)bookmarkPage:(id)sender
{
    if (self.isFavorite) {
        self.isFavorite = NO;
        [MMCOfferDAO removeOfferFromCoreData:self.offer];
        self.bookmarkButton.image = [UIImage imageNamed:@"star"];
    }
    else {
        self.isFavorite = YES;
        [MMCOfferDAO addOfferToCoreData:self.offer];
        self.bookmarkButton.image = [UIImage imageNamed:@"star_selected"];
        [self.addLabel showLabel];
    }
}

- (IBAction)openInBrowser:(id)sender
{
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:nil
                                                        delegate:self
                                               cancelButtonTitle:@"Cancelar"
                                          destructiveButtonTitle:@"Abrir Página no Safari"
                                               otherButtonTitles:nil];
    [action showInView:self.view];
}

#pragma mark - Helper Methods
- (void)loadRequest
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    __weak typeof(self) weakSelf = self;
    self.dataTask = [self.session dataTaskWithURL:[self.offer storeURL]
                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                    if (!error) {
                                        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse *) response;
                                        if (httpResp.statusCode == 200) {
                                            MMCLog(@"http status code ok");
                                            [weakSelf.webView loadData:data MIMEType:nil textEncodingName:nil baseURL:nil];
                                            
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                                MMCAbstractNavigationController *nav = (MMCAbstractNavigationController *) weakSelf.navigationController;
                                                [nav hideNotConnected];
                                            });
                                        }
                                        else {
                                            NSLog(@"error http status code %ld", (long)httpResp.statusCode);
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                                MMCAbstractNavigationController *nav = (MMCAbstractNavigationController *) weakSelf.navigationController;
                                                [nav showNotConnected];
                                            });
                                        }
                                    }
                                    else {
                                        NSLog(@"error data task %@", error);
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                            MMCAbstractNavigationController *nav = (MMCAbstractNavigationController *) weakSelf.navigationController;
                                            [nav showNotConnected];
                                        });
                                    }
                                }];
    [self.dataTask resume];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"RelatedSegue"]) {
        MMCOfferTableController *otc = (MMCOfferTableController *)segue.destinationViewController;
        otc.url = [self.offer offerURL];
        [otc prepareTableContents];
        otc.navigationItem.rightBarButtonItem = nil;
        
        if ([self.navigationController isKindOfClass:[MMCFrontNavigationController class]]) {
            [otc prepareViewTitle:@"Ofertas Relacionadas" forStyle:MMCTitleStyleDark];
        }
        else {
            [otc prepareViewTitle:@"Ofertas Relacionadas" forStyle:MMCTitleStyleLight];
        }
    }
}

#pragma mark - Action Sheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != actionSheet.cancelButtonIndex) {
        NSURL *url = self.webView.request.URL;
        [[UIApplication sharedApplication] openURL:url];
    }
}

#pragma mark - Web View Delegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if ([self.webView canGoBack]) {
        self.backButton.enabled = YES;
    }
    else {
        self.backButton.enabled = NO;
    }
    
    if ([self.webView canGoForward]) {
        self.forwardButton.enabled = YES;
    }
    else {
        self.forwardButton.enabled = NO;
    }
}

#pragma mark - NJK WebView Progress Delegate
- (void)webViewProgress:(NJKWebViewProgress *)webViewProgress updateProgress:(float)progress
{
    if (progress >= NJKInitialProgressValue) {
        [progressView setProgress:progress animated:YES];
    }
}

#pragma mark - NJK Scrollview Delegate
- (void)scrollFullScreen:(NJKScrollFullScreen *)proxy scrollViewDidScrollUp:(CGFloat)deltaY
{
    [self moveNavigtionBar:deltaY animated:YES];
    [self moveToolbar:-deltaY animated:YES];
}

- (void)scrollFullScreen:(NJKScrollFullScreen *)proxy scrollViewDidScrollDown:(CGFloat)deltaY
{
    [self moveNavigtionBar:deltaY animated:YES];
    [self moveToolbar:-deltaY animated:YES];
}

- (void)scrollFullScreenScrollViewDidEndDraggingScrollUp:(NJKScrollFullScreen *)proxy
{
    [self hideNavigationBar:YES];
    [self hideToolbar:YES];
}

- (void)scrollFullScreenScrollViewDidEndDraggingScrollDown:(NJKScrollFullScreen *)proxy
{
    [self showNavigationBar:YES];
    [self showToolbar:YES];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
    [self.scrollProxy reset];
}

@end
