//
//  MMCNewsTableController.h
//  kecaia
//
//  Created by Mario Concilio on 07/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMCOfferTableController.h"

@interface MMCNewsTableController : MMCOfferTableController

@end
