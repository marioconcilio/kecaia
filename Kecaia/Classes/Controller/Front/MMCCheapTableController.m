//
//  MMCCheapTableController.m
//  kecaia
//
//  Created by Mario Concilio on 13/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import "MMCCheapTableController.h"
#import "MMCOfferDAO.h"

@implementation MMCCheapTableController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self prepareViewTitle:@"Mais Baratos" forStyle:MMCTitleStyleDark];
    self.navigationItem.title = @"Baratos";
    
    self.url = [NSURL URLWithString:@"http://www.kecaia.com.br/mais-baratos"];
    [self prepareTableContents];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
