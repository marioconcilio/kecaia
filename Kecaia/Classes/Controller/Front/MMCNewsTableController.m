//
//  MMCNewsTableController.m
//  kecaia
//
//  Created by Mario Concilio on 07/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import "MMCNewsTableController.h"
#import "MMCOfferDAO.h"
#import "MMCTabBarController.h"

@implementation MMCNewsTableController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self prepareViewTitle:@"Últimas Novidades" forStyle:MMCTitleStyleDark];
    self.navigationItem.title = @"Novidades";
    
    self.url = [NSURL URLWithString:@"http://www.kecaia.com.br/ultimas-novidades"];
    [self prepareTableContents];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];    
    // Dispose of any resources that can be recreated.
}

@end
