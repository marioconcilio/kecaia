//
//  MMCSearchTableController.h
//  Kecaia
//
//  Created by Mario Concilio on 19/04/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMCSearchTableController : UITableViewController <UISearchBarDelegate>

@property (nonatomic, strong) NSString *searchText;
@property (nonatomic, strong) NSMutableArray *list;

- (NSString *)dataFilePath;
- (void)addStringToList:(NSString *)string;
- (void)applicationDidEnterBackground:(NSNotification *)notification;

@end
