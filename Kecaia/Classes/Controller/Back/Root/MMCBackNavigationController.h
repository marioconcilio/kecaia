//
//  MMCBackNavigationController.h
//  Kecaia
//
//  Created by Mario Concilio on 03/04/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMCAbstractNavigationController.h"

@interface MMCBackNavigationController : MMCAbstractNavigationController

@end
