//
//  MMCSearchTableController.m
//  Kecaia
//
//  Created by Mario Concilio on 19/04/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <SWRevealViewController/SWRevealViewController.h>

#import "MMCSearchTableController.h"
#import "MMCOfferTableController.h"

#define kDataFileName   @"search.plist"
#define kMaxSizeList    15

@implementation MMCSearchTableController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *filePath = [self dataFilePath];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
//        NSError *error;
//        [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
//        if (error) {
//            NSLog(@"error deleting file: %@", error);
//        }
        self.list = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
    }
    else {
        self.list = [[NSMutableArray alloc] init];
    }

    self.tableView.backgroundColor = [UIColor customBlack];
    self.tableView.separatorColor = [UIColor customWhite];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tableview"]];
    
    UISearchBar *searchBar = [[UISearchBar alloc] init];
    searchBar.barStyle = UIBarStyleBlack;
    searchBar.delegate = self;
    searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.navigationItem.titleView = searchBar;
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancelar"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(cancel)];
    self.navigationItem.rightBarButtonItem = cancelButton;
    
    UIApplication *app = [UIApplication sharedApplication];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidEnterBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:app];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Helper Methods
- (NSString *)dataFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    return [documentsDirectory stringByAppendingPathComponent:kDataFileName];
}

- (void)addStringToList:(NSString *)string
{
    for (NSString *str in self.list) {
        if ([string isEqualToString:str]) {
            return;
        }
    }
    
    if (self.list.count == kMaxSizeList) {
        [self.list removeObjectAtIndex:0];
    }
    
    [self.list addObject:string];
}

- (void)applicationDidEnterBackground:(NSNotification *)notification
{
    [self.list writeToFile:[self dataFilePath] atomically:YES];
}

- (void)cancel
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    [self setNeedsStatusBarAppearanceUpdate];
    [self.revealViewController setFrontViewPosition:FrontViewPositionRightMost animated:YES];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"SearchSegue"]) {
        NSIndexPath *selectedCellPath = [self.tableView indexPathForSelectedRow];
        if (selectedCellPath) {
            NSUInteger count = self.list.count - 1;
            self.searchText = [self.list objectAtIndex:count - selectedCellPath.row];
        }
        
        NSString *stringURL = [NSString stringWithFormat:@"%@%@", kMMCDomainSearch, [self.searchText stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
        MMCLog(@"%@", stringURL);
        
        MMCOfferTableController *otc = (MMCOfferTableController *) segue.destinationViewController;
        otc.url = [NSURL URLWithString:stringURL];
        [otc prepareViewTitle:self.searchText forStyle:MMCTitleStyleLight];
        [otc prepareTableContents];
        otc.navigationItem.rightBarButtonItem = nil;
    }
}

#pragma mark - Search Delegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    self.searchText = searchBar.text;
    [self addStringToList:searchBar.text];
    searchBar.text = nil;
    [self.tableView reloadData];
    [self performSegueWithIdentifier:@"SearchSegue" sender:self];
}

#pragma mark - Table View Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.list count];
}

#pragma mark - Table View Data Source
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SearchCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.font = [UIFont fontWithName:kMMCFontLight size:18.0];
    
    // Configure the cell...
    // data to cell
    NSUInteger row = [indexPath row];
    NSUInteger count = self.list.count - 1;
    cell.textLabel.text = [self.list objectAtIndex:count - row];
    cell.textLabel.textColor = [UIColor customWhite];
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

@end
