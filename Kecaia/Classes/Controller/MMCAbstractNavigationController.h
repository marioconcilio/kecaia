//
//  MMCAbstractNavigationController.h
//  Kecaia
//
//  Created by Mario Concilio on 27/04/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MMCConnectionLabel;
@interface MMCAbstractNavigationController : UINavigationController

@property (nonatomic, strong) MMCConnectionLabel *conn;

- (void)showNotConnected;
- (void)hideNotConnected;

@end
