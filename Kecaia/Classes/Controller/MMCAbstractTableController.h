//
//  MMCAbstractTableController.h
//  Kecaia
//
//  Created by Mario Concilio on 05/04/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NJKScrollFullScreen/NJKScrollFullScreen.h>

typedef NS_ENUM(NSInteger, MMCTitleStyle) {
    MMCTitleStyleLight,
    MMCTitleStyleDark,
};

@interface MMCAbstractTableController : UITableViewController <NJKScrollFullscreenDelegate>

@property (strong, nonatomic) NSArray *list;
@property (strong, nonatomic) NJKScrollFullScreen *scrollProxy;

- (void)prepareViewTitle:(NSString *)title forStyle:(MMCTitleStyle)style;

@end
