//
//  MMCAbstractNavigationController.m
//  Kecaia
//
//  Created by Mario Concilio on 27/04/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import "MMCAbstractNavigationController.h"
#import "MMCConnectionLabel.h"

@implementation MMCAbstractNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.conn = [[MMCConnectionLabel alloc] initWithNavigationController:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showNotConnected
{
    [self.conn showLabel];
}

- (void)hideNotConnected
{
    if (self.conn.isVisible) {
        [self.conn hideLabel];
    }
}

@end
