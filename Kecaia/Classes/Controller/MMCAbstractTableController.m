//
//  MMCAbstractTableController.m
//  Kecaia
//
//  Created by Mario Concilio on 05/04/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <NJKScrollFullScreen/UIViewController+NJKFullScreenSupport.h>
#import "MMCAbstractTableController.h"

@implementation MMCAbstractTableController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.scrollProxy = [[NJKScrollFullScreen alloc] initWithForwardTarget:self];
    self.tableView.delegate = (id) self.scrollProxy;
    self.scrollProxy.delegate = self;
    
    self.tableView.backgroundColor = [UIColor customWhite];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self showNavigationBar:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helper Methods
- (void)prepareViewTitle:(NSString *)title forStyle:(MMCTitleStyle)style
{
    UIFont *fontLight = [UIFont fontWithName:kMMCFontLight size:20.0];
    UIFont *fontBold = [UIFont fontWithName:kMMCFontBold size:20.0];
    
    NSMutableAttributedString *attributeTitle = [[NSMutableAttributedString alloc] initWithString:title];
    
    NSRange firstSpace = [title rangeOfString:@" "];
    if (firstSpace.location != NSNotFound) {
        [attributeTitle addAttribute:NSFontAttributeName value:fontLight range:NSMakeRange(0, firstSpace.location)];
    }
  
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160, 64)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = fontBold;
    
    switch (style) {
        case MMCTitleStyleLight:
            titleLabel.textColor = [UIColor customWhite];
            break;
            
        case MMCTitleStyleDark:
            titleLabel.textColor = [UIColor customBlack];
            break;
    }
    
    titleLabel.attributedText = attributeTitle;
    self.navigationItem.titleView = titleLabel;
}

#pragma mark - Table View Data Source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.list count];
}

#pragma mark - ScrollView
- (void)scrollFullScreen:(NJKScrollFullScreen *)proxy scrollViewDidScrollUp:(CGFloat)deltaY
{
    if (self.list) {
        [self moveNavigtionBar:deltaY animated:YES];
    }
}

- (void)scrollFullScreen:(NJKScrollFullScreen *)proxy scrollViewDidScrollDown:(CGFloat)deltaY
{
    if (self.list) {
        [self moveNavigtionBar:deltaY animated:YES];
    }
}

- (void)scrollFullScreenScrollViewDidEndDraggingScrollUp:(NJKScrollFullScreen *)proxy
{
    if (self.list) {
        [self hideNavigationBar:YES];
    }
}

- (void)scrollFullScreenScrollViewDidEndDraggingScrollDown:(NJKScrollFullScreen *)proxy
{
    if (self.list) {
        [self showNavigationBar:YES];
    }
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
    [self.scrollProxy reset];
    [self showNavigationBar:YES];
}

@end
