//
//  main.m
//  kecaia
//
//  Created by Mario Concilio on 06/03/14.
//  Copyright (c) 2014 Mario Concilio. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MMCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        if (getenv("NSZombieEnabled")) {
            NSLog(@"*** ZOMBIE ENABLED ***");
        }
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MMCAppDelegate class]));
    }
}
